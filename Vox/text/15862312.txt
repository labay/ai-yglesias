5 times Trump tweeted that he would never cut Medicaid
He’s cutting Medicaid. A lot. 
Way back on June 8, 2014, Donald Trump — who by then had long since established himself in conservative circles as a prominent voice in birther conspiracy theories and anti-immigrant demagoguery — staked out a strikingly heterodox position: He wanted to save entitlement programs.
As a candidate during the 2015 GOP primary process, it was a theme he returned to. When former Arkansas Gov. Mike Huckabee also stepped up with a promise to protect Social Security and Medicare, Trump claimed total ownership of the pro-entitlement position — vowing specifically to defend Medicaid as well.
Trump portrayed this issue as a key differentiator. Defending Medicaid was a central plank of his pitch for American greatness.
He even specifically referred to Ohio Gov. John Kasich as being excessively right-wing on the Medicaid issue. 
Now, of course, Trump has lined up behind a Senate health care bill that features draconian cuts to Medicaid:
Kasich says he thinks the cuts go too far. But Trump himself doesn’t tweet about Medicaid anymore. He just keeps saying that “Obamacare is dead.” 
That’s not really true, but even if you think it is, it’s true Medicaid certainly isn’t dead. But the Senate health care bill will kill it, and will do so in order to finance a large tax cut for wealthy people — something Trump’s economic team also promised not to do. 