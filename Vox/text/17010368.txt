Democrats just flipped a Republican-held Florida state legislative seat in a district Trump won
That’s the 36th pickup since Trump’s election.
Democrat Margaret Good defeated Republican James Buchanan by a 52-45 margin in a special election for Florida House District 72, marking the 36th state legislative seat Democrats have flipped since Donald Trump’s election.
Trump carried the district by a 51-46 margin, so this isn’t a huge upset, but it does continue a pattern of downballot Democratic candidates performing substantially be
tter in 2017 and 2018 than Hillary Clinton did in 2016. 

The win also doesn’t do anything to meaningfully alter the balance of power in Tallahassee, which remains heavily tilted toward the GOP, but it suggests that the national political environment continues to be favorable to Democrats as they look ahead to the midterms in November. Democrats are looking to pick up a couple of House seats and ideally the governor’s mansion in Florida, which would give them a voice in the next redistricting process. 
Vote totals are not yet available for two other special elections being held tonight, both in much redder districts in Georgia and Oklahoma, where the odds of a GOP win are overwhelming.
Long story short, despite Trump’s somewhat improved poll numbers, he remains historically unpopular for a president early in his second year, and Republicans are paying the price at the polls.
According to an extremely useful comprehensive spreadsheet compiled by Daily Kos, across 70 special elections in 2017, Democrats ran 10 points ahead of Clinton and 7 points ahead of Obama’s 2012 results. Those numbers have accelerated into 2018. Across 12 races, Democrats are running 23 points ahead of Clinton and 8 points ahead of Obama. 
Historically speaking, special election results usually are somewhat predictive of midterm general election outcomes, though I don’t think anyone believes it’s realistic for Democrats to obtain a nationwide 23-point swing relative to Clinton’s numbers. 
Meanwhile, the special elections are already having real-world impact. 
Wisconsin Gov. Scott Walker has decided to leave a number of formerly GOP-held seats vacant rather than schedule special elections his party might lose, national Republicans are pushing the panic button on an upcoming special House election in Pennsylvania, and GOP leadership is letting scandal-plagued Rep. Blake Farenthold stick around in his seat rather than risk a special election.
