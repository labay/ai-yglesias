The Weeds: gentrification, a budget deal, and the decline of floating voters
On the latest episode of The Weeds, Ezra and Sarah bug me about gentrification — a thorny problem with a paradoxical solution: Cities need to change even faster. We also take the deepest dive we can muster into the budget deal, details of which were breaking just as of recording time, and Ezra enlightens us with some academic research on why there just aren't as many voters up for grabs these days as there used to be.
As always, you can subscribe to The Weeds on iTunes, rate us there, and reach out with feedback at weeds@vox.com. We will be releasing another episode one week from today, on Christmas, so plan to spend your holiday nerding out.

Show notes: