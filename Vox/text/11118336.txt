3 winners and 2 losers in the CNN Republican debate
Five candidates took the stage in Houston to debate whom the Republican Party should nominate for president, but only three of them really mattered and one of them — frontrunner Donald Trump — truly dominated the evening. He talked more than any of his rivals and was talked about more than any of his rivals, because the Republican Party has finally gotten serious about the idea that unless something drastic changes soon, he is likely to be the GOP nominee.
Thursday night was the first debate to feature sustained multidirectional attacks on Trump with little infighting between the non-Trump candidates. It was also the first debate in which Marco Rubio squarely defined himself as the anti-Trump, hitting him both explicitly and implicitly on both policy and personal matters.
Here's who won and lost in the fracas that ensued.
Winner: Donald Trump
Inside the echo chamber of establishment Republican political operatives and their friends in the press, Marco Rubio triumphed in the debate by finally jabbing at Trump. But before rushing to join that consensus, note that this exact same echo chamber proclaimed Rubio the winner of a number of early debates for taking precisely the opposite strategic approach.
The Thursday night jabs were decent, but in a fundamental sense they didn't even begin to challenge the core of Trumpism. Trump says the Republican Party ought to adopt a more nationalist stance on immigration, trade, and foreign entanglements, and a large share of the Republican rank and file seem to agree.
Rubio hit Trump for having been fined years ago for having employed unauthorized workers, which is fine, but there's no way he can get to Trump's right on the underlying issue of immigration. Rubio needled Trump about how repetitive and uninterested in policy detail he is, and clearly got under his skin. He hit him for being insufficiently fanatical in his devotion to Israel and his opposition to universal health care. He brought up the ignominious failure of Trump University. He was uptempo, ad-libbed a little, and generally lifted the spirits of Republicans who've been annoyed or horrified by Trump's rise.
But at this point, Trump already has a commanding lead in the polls. And from the standpoint of someone who's already bought into the idea of President Trump, it's not clear what these attacks amount to. Trump's pitch is that he's a ruthless businessman who now wants to change careers and exercise his ruthlessness on behalf of the (implicitly white and Christian) traditional definition of the American nation. Nothing Rubio said or did really challenged any of the key premises of that pitch.
Indeed, in some ways it simply reenforced them. Do Trump's constituents worry that the next president will be an insufficiently zealous advocate for Israel's interests or for America's? Do they want a wheezy discussion of health care policy, or do they want to hear about making Mexico pay for a border wall?
Winner: Mitt Romney
When Mitt Romney raised the issue of why Donald Trump hasn't released his income tax returns earlier this week, it seemed like not much more than light trolling grounded in historical irony.
But CNN followed Romney's lead and asked Trump about it, which led to a somewhat bizarre reply from Trump in which he argued that he can't release his returns because he's being audited.
 
 



This defense does not appear to be legally accurate, and even if it somehow is, the fact of the audits seem damning themselves. The actual exchange didn't seem all that dramatic or necessarily damaging, but the unsatisfying nature of the answer essentially guarantees that the question will keep being asked as long as Trump is a candidate. Romney managed to shift the course of the campaign more than many of the candidates who've actually been in the race.
Winner: Planned Parenthood
Every Republican candidate opposes abortion and wants to defund Planned Parenthood, of course. But current frontrunner Donald Trump actually spoke up for the organization in general, saying, "There are millions of women going through Planned Parenthood that are greatly helped," citing treatments for cervical cancer and breast cancer as examples.
"I would defund it because I'm pro-life," Trump said, "but millions of women are helped by Planned Parenthood."
It was far and away the strongest defense of Planned Parenthood's work that any Republican has offered in a long time, and it came from the guy who's on top of the polls.
Loser: Wolf Blitzer
You'd expect a professional television personality to be able to do better than this:

Loser: Ted Cruz
Rubio's attacks on Trump hardly seemed devastating, but they were well-delivered and at a couple of points genuinely funny, and they certainly impressed the entire establishment Republican universe. In short, Rubio further cast the election as a two-man race between a paradoxically establishment-backed underdog and Trump as the outsider frontrunner.
That leaves no real role for Cruz to play, and voters who want to see Cruz in the White House increasingly need to think about whether casting a vote for Cruz amounts to wasting it. If you agree with Cruz that Trump is an inauthentic conservative who can't be trusted to implement orthodox policies, you should probably vote for Rubio. And if you agree with Cruz that Rubio is a shady establishment pawn who'll sell the base down the river for a bucket of amnesty, you should probably vote for Trump.