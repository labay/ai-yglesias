Mark Krikorian: "Donald Trump is unfit to be president … and I'm going to vote for him anyway"
"Donald Trump is unfit to be president," writes Mark Krikorian of the conservative Center for Immigration Studies at National Review's blog, the Corner.
The next sentence is: "And I'm going to vote for him anyway."
Presidents lead a big team
The reasons he gives are pretty simple. Presidents appoint people to important jobs. Krikorian specifically cites the Supreme Court, which would be on anyone's list, and rather idiosyncratically names the Civil Rights Division of the Justice Department as the other big one because "the division is Left's most potent weapon in imposing its will on every city and town, every baker and florist, every church and synagogue in the nation."
Under renewed Democratic leadership, Krikorian says, "the ACLU would continue to wage lawfare against Americans on Americans' own dime, irreversibly altering the fabric of our society."
This is what most conservatives will do
On the one hand, this bit about the Civil Rights Division is unhinged and a pretty good example of how the funhouse mirror of conservative movement politics creates a culture friendly to Trump's own brand of wild politics.
On the other hand, Krikorian is articulating a perfectly sensible point about appointments in general.
In the voting booth, you pick a president. In reality, you are picking a whole presidential administration. A Hillary Clinton administration will be staffed by reliable Democratic Party figures. Trump is enough of a weirdo to cast some doubt on the proposition that he will staff his administration with reliable Republican Party figures, but it still seems like he probably will. And the further you go down the food chain — not the attorney general but the assistant attorney general for civil rights — the more likely you are to find generic party figures in jobs.
Since most issues are obscure most of the time, in a day-to-day sense these staffing decisions tend to matter more than the question of what the president "really believes" or even knows anything about.
So for most people, a pretty blind partisan vote ends up seeming compelling no matter what.
Watch: What do conservatives really think of Donald Trump?