The Weeds: the productivity slowdown, TrumpCare, and better textbooks
In this week's episode, Sarah, Ezra, and Matt talk about why Silicon Valley's technical marvels aren't visible in economic statistics, dive into the details of Donald Trump's surprisingly banal health plan, and tackle some new research on improving teaching by improving textbooks.

Show notes: