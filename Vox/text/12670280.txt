Donald Trump just made his worst tweet yet
Friday night, Nykea Aldridge, a 32-year-old woman living in Chicago was shot and killed while pushing a stroller in an apparent accident. The shooters were aiming for another man, and she was caught in the crossfire.
It’s an extraordinarily sad story but not necessarily one that would attract attention outside the Chicagoland area — except for the fact that the victim is the first cousin of NBA star Dwyane Wade, who tweeted about it.
Wade, however, wasn’t the only prominent Twitter user to take note of the tragedy.
Donald Trump also thought that the death of an innocent woman was noteworthy, though in his case the idea was to make an ax-grinding partisan point:



It’s extraordinarily unlikely that any actual African-American voters will react to violent crime in Chicago by voting for Trump. (He also misspelled Wade’s first name in a previous version of this tweet.)
But as Dara Lind wrote last week, Trump’s nominal African-American outreach is really aimed at white voters. His racism hasn’t just turned off nonwhite voters. It’s turned off many white moderates and Republicans who would probably vote for any other Republican presidential nominee over Hillary Clinton, but are worried that Trump is simply too offensive, too bullying, and too bitter to lead the free world.
Unlike nonwhite voters, though, wavering Republicans are still looking for a reason to vote Trump. And Trump’s show of racial unity is what they need to feel that, once again, Republicans have the high ground when it comes to race and identity.
This particular tweet, however, though arguably aimed at softening Trump’s image on race, isn’t going to too much of anything to debunk the notion that he simply lacks a certain level of human empathy that we expect from a president. An innocent woman is dead. Children have lost their mother. A family is grieving. And Trump is making a boast about his electoral performance. From any other candidate it would be considered shocking, but as E.J. Dionne has written, "staying shocked" is a challenge as we watch the Trump campaign unfold.
Watch: What the media gets wrong about Trump voters